---
layout: home
permalink: /
title: "Corvid Investigations: Providing criminal defense investigation in Philadelphia & throughout the Commonwealth of Pennsylvania"
main_header: "Corvid Investigations"
subheader: "Providing criminal defense investigation in Philadelphia & throughout the Commonwealth of Pennsylvania"
main_description: "Owner & Investigator Owen Schmidt began his investigative career with the DC Public Defender Service. He believes that everyone deserves thorough, high quality investigation when faced with criminal charges."
cta_header: "Get in touch"
cta_description: "If you've been charged with a crime in Pennsylvania, Corvid Investigations can provide important assistance to your defense attorney. Email or call for a free consultation about your situation."
cta_button_text: "Email us"
cta_button_link: "mailto:corvidinvestigations@gmail.com"
---
